from django.shortcuts import render

def index_main(request):
    return render(request, 'countdown/index.html')