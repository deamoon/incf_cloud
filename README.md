# INCF experiment cloud platform #

Ivanovskii Dmitrii, GSOC application

### Title ###
A comprehensive Python-R online tools suite

### Other services ###

* [Wakari.io](https://wakari.io/)
* [CloudCV](http://www.cloudcv.org/)
* [BigML](https://bigml.com/)
* [Azure](http://azure.microsoft.com/en-us/services/machine-learning/)
* [Google Prediction](https://cloud.google.com/prediction/)
* [ElasticR](https://www.elasticr.com/welcome)
* [Rcloud](http://www.ebi.ac.uk/Tools/rcloud/)

### Stack technology ###

* OpenStack, AWS, DigitalOcean?
* Ubuntu 14.04 server x64
* Nginx
* uWSI, Gunicorn?
* Django 1.7
* Docker, Rokcer(Docker + R), Docker(python + iPython)
* Celery
* Rabbit MQ
* Bootstrap, jQuery
* Angular, Backbone, pure Javascript, React?
* Supervisor

### Continuous integration ###

* [Codeship.io](https://codeship.com/)

### Contrubutors ###

* Ivanovskii Dmitrii, deamoon, dima-iv(at)mail.ru